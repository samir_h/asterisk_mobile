
import React, { Component } from 'react';
import {
  AppRegistry,
  ScrollView,
    FlatList,
    Text,
    StyleSheet,
    View,
    ActivityIndicator,
} from 'react-native';

import { ListItem, List, Icon,FormInput,FormLabel } from 'react-native-elements';
import {Fetch} from 'react-native-fetch';

export default class ProductsComponent extends Component {
    constructor(props){
        super(props);
        this.state={
            isLoading:true,
            keyword:'',
            products:'',
        };

    };

    componentDidMount(){
        return fetch('https://samir-hajdarpasic.000webhostapp.com/api/get_products_api')
            .then((response) =>response.json())
            .then((responseJson)=>{
                console.log(responseJson);
                this.setState({
                    products:responseJson.products,
                    isLoading: false
                });
            })
            .catch((error) => {
                console.error(error);
            });
    };
    setText(text){
        this.setState({
            keyword:text
        });
    }


    search(){
            this.setState({
              isLoading:true,
            });
          return fetch('https://samir-hajdarpasic.000webhostapp.com/api/get_products_api/'+this.state.keyword)
              .then((response) =>response.json())
              .then((responseJson)=> {
                  this.state = {
                      products: responseJson.products,
                      isLoading:false
                  };
              });
    };



  render() {
      if (this.state.isLoading) {
          return (
              <View style={{flex: 1, paddingTop: 20}}>
                  <ActivityIndicator />
              </View>
          );
      }
    return (
        <View>
            <FormLabel>Search</FormLabel>
            <FormInput onChangeText={(text) =>this.setText(text)} onSubmitEditing={(text) => this.search()}/>

            <View style={styles.text_header}>
                <Text style={styles.right_text1}>Product</Text>
                <Text style={styles.right_text2}>Price</Text>
            </View>
            <ScrollView>
                <List>
                    <FlatList
                        data={this.state.products}
                        renderItem={({item}) =>(
                            <ListItem
                                title={item.name}
                                rightTitle={item.price}
                                rightIcon={{name:'watch'}}
                                rightTitleStyle={{color:'black'}}
                                hideChevron={true}
                            />
                        )}
                        keyExtractor={item => item.id}
                    />
                </List>
            </ScrollView>
        </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 22
    },
    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    },

    price:{
    },
    text_header:{
        alignItems:'stretch',
        flexDirection:"row",
    },

    right_text1:{
        alignSelf:'flex-end',
        flex:1,
        paddingLeft:20
    },

    right_text2:{
        alignSelf:'flex-end',
        flex:0,
        paddingRight:10
    },
});
AppRegistry.registerComponent('ProductsComponent', () => ProductsComponent);
