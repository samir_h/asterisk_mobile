
import React, { Component } from 'react';
import {
    AppRegistry,
    ScrollView,
    FlatList,
    Text,
    StyleSheet,
    View,
    ActivityIndicator,
    Alert,
} from 'react-native';

import { ListItem, List, Icon,FormInput,FormLabel,Button } from 'react-native-elements';
import {Fetch} from 'react-native-fetch';


export default class Sales extends Component {

    constructor(props){
        super(props);
        this.state={
            isLoading:true,
        };

    };

    componentDidMount(){
        return fetch('https://samir-hajdarpasic.000webhostapp.com/api/get_sales')
            .then((response) =>response.json())
            .then((responseJson)=>{
                console.log(responseJson);
                this.setState({
                    sales:responseJson.sales,
                    isLoading: false
                });
            })
            .catch((error) => {
                console.error(error);
            });
    };

    raportiDitor(){
    
    }

    render() {
      if (this.state.isLoading) {
          return (
              <View style={{flex: 1, paddingTop: 20}}>
                <ActivityIndicator />
              </View>
          );
      }
      return (
          <View>
            <Button small raised backgroundColor={'#FFC108'} title="Raporti Ditor" onPress={()=>this.raportiDitor()} buttonStyle={styles.btn}/>
            <Button small raised backgroundColor={'#8AC24A'} title="Raporti Javor" onPress={()=>this.raportiDitor()} buttonStyle={styles.btn}/>
            <Button small raised backgroundColor={'#009588'} title="Raporti Mujor" onPress={()=>this.raportiDitor()} buttonStyle={styles.btn}/>
            <Button small raised backgroundColor={'#2095F3'} title="Raporti Vjetor" onPress={()=>this.raportiDitor()} buttonStyle={styles.btn}/>
            <ScrollView>
              <View style={styles.text_header}>
                <Text style={styles.right_text1}>Product</Text>
                <Text style={styles.right_text2}>Quantity</Text>
                <Text style={styles.right_text2}>Price</Text>
              </View>
              <List>
                <FlatList
                    data={this.state.sales}
                    renderItem={({item}) =>(
                        <ListItem
                            title={item.product}
                            rightTitle={item.quantity_sold + "       "+item.price_sold}
                            rightTitleStyle={{color:'black'}}
                            hideChevron={true}
                        />
                    )}
                    keyExtractor={item => item.id}
                />
              </List>
            </ScrollView>
          </View>
      );
  }
}

const styles= StyleSheet.create({
    text_header:{
        alignItems:'center',
        flexDirection:"row",
    },

    right_text1:{
      alignSelf:'flex-end',
        flex:1,
        paddingLeft:20
    },

    right_text2:{
        alignSelf:'flex-end',
        flex:0,
        paddingRight:10
    },


    btn_header:{
        flexDirection:'row',
        paddingTop:10,
        alignItems:'center'
    },

    btn:{
        marginBottom:5,
        padding:10,
    }

});
AppRegistry.registerComponent('Sales', () => Sales);
