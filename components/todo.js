
import React, { Component } from 'react';
import {
    AppRegistry,
    ScrollView,
    FlatList,
    Text,
    StyleSheet,
    View,
    ActivityIndicator,
} from 'react-native';
import CheckBox from 'react-native-check-box';
import { ListItem, List, Icon,FormInput,FormLabel,Button } from 'react-native-elements';
import {Fetch} from 'react-native-fetch';

export default class ToDO extends Component {
    constructor(props){
        super(props);
        this.state={
            isLoading:true,
            keyword:'',
            tasks:null,
        };

    };

    componentDidMount(){
        return fetch('https://samir-hajdarpasic.000webhostapp.com/api/get_tasks')
            .then((response) =>response.json())
            .then((responseJson)=>{
                console.log(responseJson);
                this.checkStatus(responseJson);
                this.setState({
                    isLoading: false
                });

            })
            .catch((error) => {
                console.error(error);
            });
    };

    checkStatus(data){

        let temp=data;
        for(let i=0;i< temp.length;i++){
           if(temp[i].status==0){
               temp[i].status=false;
           }

            if(temp[i].status==1){
                temp[i].status=true;
            }
        }

        this.setState({
            tasks:temp
        })
        console.log(data)
    };

    changeSate(task){
        fetch('https://samir-hajdarpasic.000webhostapp.com/api/change_task', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id: task.id,
            })
        }).then((response)=>response.json())
            .then((resJson)=>console.log(resJson))
    }

    setText(text){
        this.setState({
            keyword:text
        });
    }

    addItem(){
        fetch('https://samir-hajdarpasic.000webhostapp.com/api/add_task', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name: this.state.keyword,
            })
        }).then((response)=>response.json())
            .then((resJson)=>{
                temp = this.state.tasks;
                temp.push(resJson);
                this.setState({
                    tasks:temp
                });
            })
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }
        return (
            <View>

                    <FormLabel>New Task</FormLabel>
                <View style={{flexDirection:'row',marginBottom:20}}>
                    <FormInput onChangeText={(text) =>this.setText(text)} inputStyle={{width:240}}/>
                    <Button title="Add" raised small backgroundColor={'#009588'} onPress={()=>this.addItem()} buttonStyle={{width:50,alignSelf:'flex-end'}}/>
                </View>
                <View style={styles.text_header}>
                    <Text style={styles.right_text1}>Status</Text>
                    <Text style={styles.right_text2}>Name</Text>
                </View>
                <ScrollView>
                    <List containerStyle={{height:400}}>
                        <FlatList

                            data={this.state.tasks}
                            renderItem={({item}) =>(
                                <CheckBox style={{marginLeft:20,marginRight:20, backgroundColor:'lightgrey',margin:5,padding:5}}
                                    onClick={()=>this.changeSate(item)}
                                    isChecked={item.status}
                                    rightText={item.name}
                                />
                            )}
                            keyExtractor={item => item.id}
                        />
                    </List>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 22
    },
    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    },

    text_header:{
        alignItems:'stretch',
        flexDirection:"row",
    },

    right_text1:{
        alignSelf:'flex-end',
        flex:1,
        paddingLeft:20
    },

    right_text2:{
        alignSelf:'flex-end',
        flex:3,
        paddingRight:10
    },

});
AppRegistry.registerComponent('ToDO', () => ToDO);
