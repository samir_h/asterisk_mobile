import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
    TouchableHighlight,
    Button

} from 'react-native';
import {
  StackNavigator,
  TabNavigator,
  DrawerNavigator
} from 'react-navigation';

import ProductsComponent from './components/products';
import Sales from './components/sales';
import ToDo from './components/todo';
import Icon from "react-native-vector-icons/RNIMigration";


export const Drawer = DrawerNavigator({

    Products:{
        screen: ProductsComponent,
        navigationOptions:{
            headerTitle:'Products',
        }
    },

    Sales:{
        drawerLabel:'Selling History',
        screen: Sales,
        navigationOptions:{
            headerTitle:'Selling History',
        }
    }
    ,

    ToDo:{
        screen:ToDo,
        navigationOptions:{
            headerTitle:'ToDo List'
        }

    }
});
export const Nav = StackNavigator({
  Main: {
      screen: Drawer,
      navigationOptions:(navigation)=>({
          headerStyle:{
              backgroundColor:'#16AB9E'
          },
          //headerLeft:<Button title="<" backgroundColor='#16AB9E' onPress=""/>,

         // headerLeftIcon:<Icon name="menu"/>
      })
  }
});

